package prog2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import prog2.vista.ExcepcioClub;

public class LlistaActivitats implements InActivitatList, Serializable {
    
    private ArrayList<Activitat> llista;
    
    /**
     * Constructor predeterminat.
     */
    public LlistaActivitats(){
        this.llista = new ArrayList<Activitat>();
    }
    
    /**
     * Retorna un string amb els toString de les activitats de la llista.
     */
    @Override
    public String toString() {
        String output = "Llista d'activitats:\n===============\n";
        int count = 1;

        Iterator<Activitat> it = llista.iterator();
        while (it.hasNext()) {
            output += String.format("[%d] %s \n", count, it.next().toString());
            count += 1;
        }

        return output;
    }

    @Override
    public void addActivitat(Activitat activitat) {
        llista.add(activitat);  
    }

    @Override
    public float calculaPreuActivitats() {
        float preu = 0;
        for (Activitat activitat : llista ) {
            preu += activitat.getPreu();
        }
        return preu;
    }

    @Override
    public void clear() {
        llista.clear();
    }

    @Override
    public Activitat getAt(int position) throws ExcepcioClub {
        if (position>llista.size()){
            throw new ExcepcioClub("Index out of bounds.");
        }
        return llista.get(position);
    }

    @Override
    public int getSize() {
        return llista.size();
    }

    @Override
    public boolean isEmpty() {
        return llista.isEmpty();
    }

    /**
     * Elimina una activitat de la llista, llança error si la llista esta buida o no s'ha trobat la activitat 
     * @param Activitat
     */
    @Override
    public void removeActivitat(Activitat activitat) throws ExcepcioClub {
        if (!llista.isEmpty()){
            if (!llista.remove(activitat)) {
                throw new ExcepcioClub("No sha trobat la activitat.");
            }
        } else {
            throw new ExcepcioClub("No es pot eliminar una activitat perquè la llista està buida!");
        }
        
    }

}
