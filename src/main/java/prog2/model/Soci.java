package prog2.model;

import java.io.Serializable;

import prog2.vista.ExcepcioClub;

public abstract class Soci implements InSoci, Serializable {

    private String _name;
    private String _dni;
    private float _quotaBase;
    private LlistaActivitats _activitatsRealitzades;

    
    /**
     * Constructor inicialitza les variables i la llista d'activitats amb una llista buida.
     * @param _name
     * @param _dni
     */
    public Soci(String _name, String _dni) {
        this._name = _name;
        this._dni = _dni;
        _activitatsRealitzades = new LlistaActivitats();
    }

    @Override
    public LlistaActivitats getActivitatsRealitzades() {
        return _activitatsRealitzades;
    }

    @Override
    public String getDNI() {
        return this._dni;
    }

    @Override
    public void setDNI(String dni) {
        this._dni = dni;
    }
    
    @Override
    public String getNom() {
        return this._name;
    }
    
    @Override
    public void setNom(String nom) {
        this._name = nom;
    }
    
    @Override
    public float getQuotaBase() {
        return _quotaBase;
    };
    
    @Override
    public void setQuotaBase(float quotaBase) {
        this._quotaBase = quotaBase;
    }

    @Override
    public void afegirActivitatRealitzada(Activitat activitat) {
        this._activitatsRealitzades.addActivitat(activitat);
    }

    /**
     * Classe abstracta perque cada tipus de soci te un preu d'activitats different.
     */
    @Override
    public abstract float calculaPreuActivitats();

    /**
     * Compara dos socis per el dni.
     * @param soci
     * @return
     */
    public boolean equals(Soci soci) {
        return this._dni.equals(soci._dni);
    }

    /**
     * Classe abstracta perque cada tipus de soci imprimeix unes dades diferents differents.
     */
    public abstract String toString();

    /**
     * Classe abstracta perque cada tipus de soci calcula la factura de forma different.
     */
    public abstract float calcularFactura();

    /**
     * Classe abstracta perque cada tipus de soci es verifica de manera different.
     */
    public abstract void verifica() throws ExcepcioClub;
}
