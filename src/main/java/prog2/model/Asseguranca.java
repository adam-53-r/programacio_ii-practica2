package prog2.model;

import java.io.Serializable;

/**
 * Classe Assegurança per a representar les assegurançes dels socis estandard i junior.
 */
public class Asseguranca implements Serializable {

    /**
     * Aquest enum no es del tot necessari pero l'utilitzem per a provar altres metodes
     */
    public static enum llistaTipus {
        BASICA,
        COMPLETA
    }
    private llistaTipus _tipus;
    private float _preu;
    
    /**
     * @param _tipus
     * @param _preu
     */
    public Asseguranca(llistaTipus _tipus, float _preu) {
        this._tipus = _tipus;
        this._preu = _preu;
    }

    /**
     * @return the _tipus
     */
    public llistaTipus getTipus() {
        return _tipus;
    }

    /**
     * @param _tipus the _tipus to set
     */
    public void setTipus(llistaTipus tipus) {
        this._tipus = tipus;
    }

    /**
     * @return the _preu
     */
    public float getPreu() {
        return _preu;
    }

    /**
     * @param _preu the _preu to set
     */
    public void setPreu(float preu) {
        this._preu = preu;
    }

    @Override
    public String toString() {
        switch (_tipus) {
            case BASICA:
                return "Bàsica";
                
            case COMPLETA:
                return "Completa";
        
            default:
                return "";
        }
    }

}
