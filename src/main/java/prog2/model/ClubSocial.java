package prog2.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import prog2.vista.ExcepcioClub;

public class ClubSocial implements Serializable {

    static final float CUOTA_VIP = 50;
    static final float CUOTA_ESTANDARD = 25;
    static final float CUOTA_JUNIOR = 0;

    private LlistaSocis _llistaSocis;
    private LlistaActivitats _llistaActivitats;

    public ClubSocial() {
        _llistaSocis = new LlistaSocis();
        _llistaActivitats = InActivitatList.carregaLlistaActivitats("llistaActivitats.txt");
    }

    public int getNumSocis() {
        return _llistaSocis.getSize();
    }

    public int getNumActivitats() {
        return _llistaActivitats.getSize();
    }

    /**
     * Dona d'alta un nou soci vip amb les dades proporcionades.
     *
     * @param name
     * @param dni
     * @param descompte
     * @throws ExcepcioClub
     */
    public void donarAltaSociVip(String name, String dni, float descompte) throws ExcepcioClub {
        _llistaSocis.addSoci(new SociVip(name, dni, descompte));
    }

    /**
     * Dona d'alta un nou soci estandard amb les dades proporcionades.
     *
     * @param name
     * @param dni
     * @param tipusAsseguranca
     * @param tipusAssegurancaInput
     * @param preuAsseguranca
     * @throws ExcepcioClub
     */
    public void donarAltaSociEstandard(String name, String dni, String tipusAsseguranca, int tipusAssegurancaInput, float preuAsseguranca) throws ExcepcioClub {
        Asseguranca.llistaTipus _tipus;
        if (tipusAsseguranca.equals("BASICA")) {
            _tipus = Asseguranca.llistaTipus.BASICA;
        } else {
            _tipus = Asseguranca.llistaTipus.COMPLETA;
        }
        _llistaSocis.addSoci(new SociEstandard(name, dni, new Asseguranca(_tipus, preuAsseguranca)));
    }

    /**
     * Dona d'alta un nou soci junior amb les dades proporcionades.
     *
     * @param name
     * @param dni
     * @param tipusAsseguranca
     * @param tipusAssegurancaInput
     * @param preuAsseguranca
     * @param edat
     * @throws ExcepcioClub
     */
    public void donarAltaSociJunior(String name, String dni, String tipusAsseguranca, int tipusAssegurancaInput, float preuAsseguranca, int edat) throws ExcepcioClub {
        Asseguranca.llistaTipus _tipus;
        if (tipusAsseguranca.equals("BASICA")) {
            _tipus = Asseguranca.llistaTipus.BASICA;
        } else {
            _tipus = Asseguranca.llistaTipus.COMPLETA;
        }
        _llistaSocis.addSoci(new SociJunior(name, dni, new Asseguranca(_tipus, preuAsseguranca), edat));
    }

    /**
     * Retorna una llista de socis del tipus passat per parametre, si es ll,
     * retorna tots.
     *
     * @param type
     * @return
     * @throws ExcepcioClub
     */
    public String mostrarLlistaSocisByType(String type) throws ExcepcioClub {
        if (type.equals("all")) {
            return _llistaSocis.toString();
        }

        return _llistaSocis.getSubllistaSocisByType(type).toString();
    }

    /**
     * Elimina el soci de la llista per parámetre.
     *
     * @param dni
     * @throws ExcepcioClub
     */
    public void eliminarSoci(String dni) throws ExcepcioClub {
        _llistaSocis.removeSoci(_llistaSocis.getSoci(dni));
    }

    /**
     * Verifica els socis de la llista.
     *
     * @throws ExcepcioClub
     */
    public void verificarSocis() throws ExcepcioClub {
        _llistaSocis.verificarSocis();
    }

    /**
     * Retorna una llista amb totes les activitats.
     *
     * @return
     */
    public String mostrarLlistaActivitats() {
        return _llistaActivitats.toString();
    }

    /**
     * Retorna una llista amb totes les activitats realitzades per un soci amb el dni passat per parametre..
     * @return
     */
    public String mostrarLlistaActivitatsRealitzadesPerUnSoci(String dni) throws ExcepcioClub {
        Soci soci = _llistaSocis.getSoci(dni);
        return soci.getActivitatsRealitzades().toString();
    }

    /**
     * Afegeix una activitat de la llista a un soci donats el dni del soci i el index de la activitat dins de la llista.
     * @param dni
     * @param indexActivitat
     * @throws ExcepcioClub
     */
    public void afegirActivitatRealitzadaPerUnSoci(String dni, int indexActivitat) throws ExcepcioClub {
        _llistaSocis.getSoci(dni).afegirActivitatRealitzada(_llistaActivitats.getAt(indexActivitat-1));
    }

    /**
     * Calcula el total de la factura del soci amb el dni pasat per parametre.
     * @param dni
     * @return
     * @throws ExcepcioClub
     */
    public float totalFacturaSoci(String dni) throws ExcepcioClub{
        Soci soci = _llistaSocis.getSoci(dni);
        
        if ( soci instanceof SociVip ) {
            SociVip sociVip = (SociVip)soci;
            return sociVip.calcularFactura();
        }
        else if ( soci instanceof SociJunior ) {
            SociJunior sociJunior = (SociJunior)soci;
            return sociJunior.calcularFactura();
        }
        else if ( soci instanceof SociEstandard ) {
            SociEstandard sociEstandard = (SociEstandard)soci;
            return sociEstandard.calcularFactura();
        }
        else {
            throw new ExcepcioClub("Class not found.");
        }
    }


    public void modificarNomSoci(String dni, String newName) throws ExcepcioClub {
        _llistaSocis.getSoci(dni).setNom(newName);
    }

    public void modificarTipusAssegurancaSociEstandard(String dni, String newAssegurancaType) throws ExcepcioClub {
        SociEstandard sociEstandard = (SociEstandard)_llistaSocis.getSoci(dni);
        Asseguranca.llistaTipus _tipus;
        if (newAssegurancaType.equals("BASICA")) {
            _tipus = Asseguranca.llistaTipus.BASICA;
        } else {
            _tipus = Asseguranca.llistaTipus.COMPLETA;
        }
        sociEstandard.getAsseguranca().setTipus(_tipus);
    }
    
    /**
     * Serialitza la llista de socis i la guarda a un fitxer definit per parametre.
     * @param file
     * @throws Exception
     */
    public void saveToFile(File file) throws Exception {

        FileOutputStream fout = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(this._llistaSocis);

        oos.close();
        fout.close();
    }

    public void loadFromFile(File file) throws Exception {
        FileInputStream fin = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fin);
        this._llistaSocis = (LlistaSocis) ois.readObject();
        
        ois.close();
        fin.close();
    }
}
