package prog2.model;

import prog2.vista.ExcepcioClub;

public class SociVip extends Soci {

    private float _descompte;

    /**
     * @param _name
     * @param _dni
     * @param _descompte
     */
    public SociVip(String _name, String _dni, float _descompte) {
        super(_name, _dni);
        this.setQuotaBase(ClubSocial.CUOTA_VIP);
        this._descompte = _descompte;
    }

    @Override
    public float calcularFactura() {        
        return getQuotaBase() + calculaPreuActivitats();
    }

    @Override
    public float getQuotaBase() {
        return 50;
    }

    @Override
    public String toString() {
        return String.format("Nom=%s, DNI=%s, Descompte=%.2f", getNom(),getDNI(),get_descompte());
    }

    @Override
    public void verifica() throws ExcepcioClub {      
        if (_descompte > 50){
            throw new ExcepcioClub("El descompte del preu d'activitats no és correcte.");
        }
    }

    /**
     * @return the _descompte
     */
    public float get_descompte() {
        return _descompte;
    }

    /**
     * @param _descompte the _descompte to set
     */
    public void set_descompte(float _descompte) {
        this._descompte = _descompte;
    }

    /**
     * Calcula el preu de les activitats aplicant el descompte del soci.
     */
    @Override
    public float calculaPreuActivitats() {
        float preu = getActivitatsRealitzades().calculaPreuActivitats();
        return preu - preu * _descompte/100;
    }
    
}
