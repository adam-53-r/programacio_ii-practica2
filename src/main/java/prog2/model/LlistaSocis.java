package prog2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import prog2.vista.ExcepcioClub;

public class LlistaSocis implements InSociList, Serializable {
    
    private ArrayList<Soci> _llista;
    private int _size;
    
    
    /**
     * Constructor predeterminat amb una mida maxima de 100 socis.
     */
    public LlistaSocis() {
        this._llista = new ArrayList<Soci>();
        this._size = 100;
    }
    
    /**
     * Constructor amb una mida maxima pasada per parametre.
     * @param _size
     */
    public LlistaSocis(int _size) {
        this._llista = new ArrayList<Soci>();
        this._size = _size;
    }
    
    /**
     * Retorna un String amb els toString de tots es socis de la llista.
     */
    @Override
    public String toString() {
        
        String _output = "";
        int _count = 1;

        Iterator<Soci> it = _llista.iterator();
        while (it.hasNext()) {
            _output += String.format("[%d] %s \n", _count, it.next().toString());
            _count += 1;
        }

        return _output;
    }

    /**
     * Afegeix un soci pasat per parametre, llança un error si la llista esta plena o si el soci ja esta a la llista.
     */
    @Override
    public void addSoci(Soci soci) throws ExcepcioClub {
        
        if (_llista.size() > _size-1) {
            throw new ExcepcioClub("La llista esta plena.");
        }

        if (this.contains(soci)) {
            throw new ExcepcioClub("El soci ja esta a la llista.");
        }

        _llista.add(soci);

    }


    @Override
    public void clear() {
        _llista.clear();
    }

    @Override
    public boolean contains(Soci soci) {
        
        boolean _contains = false;

        Iterator<Soci> it = _llista.iterator();
        while (it.hasNext() && !_contains) {
            _contains = soci.equals(it.next());
        }

        return _contains;
    }

    /**
     * Retorna el soci a la posicio pasada per parametre, llança error si el index esta fora del rang valid de socis.
     */
    @Override
    public Soci getAt(int index) throws ExcepcioClub {
        if (index > _size || index < 0) {
            throw new ExcepcioClub("Index out of bounds.");
        }

        return _llista.get(index);
    }

    @Override
    public int getSize() {
        return _size;
    }

    /**
     * Retorna un soci de la llista amb el dni pasat per parametre, llança excepcio si no es troba el soci.
     */
    @Override
    public Soci getSoci(String dni) throws ExcepcioClub {
        boolean _found = false;
        Soci _temp = null;
        
        Iterator<Soci> it = _llista.iterator();
        while (it.hasNext() && !_found) {
            _temp = it.next();
            _found = _temp.getDNI().equals(dni);
        }

        if (!_found) {
            throw new ExcepcioClub(String.format("Soci amb DNI \"%s\" no trobat.", dni));
        }

        return _temp;
    }

    /**
     * Retorna una LlistaSocis amb els socis de la llista del tipus passat per parametre, llança error si el tipus no es valid.
     */
    @Override
    public LlistaSocis getSubllistaSocisByType(String type) throws ExcepcioClub {
        
        LlistaSocis _llistaSocis = new LlistaSocis();
        Iterator<Soci> it = _llista.iterator();
        Soci temp;
        
        switch (type) {
            case "SociEstandard":
                while (it.hasNext()) {
                    temp = it.next();
                    if ( (temp instanceof SociEstandard) && !(temp instanceof SociJunior) ) {
                        _llistaSocis.addSoci((SociEstandard) temp);
                    }
                }
                break;
        
            case "SociJunior":
                while (it.hasNext()) {
                    temp = it.next();
                    if ( temp instanceof SociJunior ) {
                        _llistaSocis.addSoci((SociJunior) temp);
                    }
                }
                break;
        
            case "SociVip":
                while (it.hasNext()) {
                    temp = it.next();
                    if ( temp instanceof SociVip ) {
                        _llistaSocis.addSoci((SociVip) temp);
                    }
                }
                break;
        
            default:
                throw new ExcepcioClub("Tipus de soci no valid.");
        }
        
        return _llistaSocis;
    }

    @Override
    public boolean isEmpty() {
        return _llista.isEmpty();
    }

    @Override
    public boolean isFull() {
        return _llista.size() == _size;
    }

    @Override
    public void removeSoci(Soci soci) throws ExcepcioClub {
        if ( !_llista.remove(soci) ) {
            throw new ExcepcioClub("El soci no existeix.");
        }
    }

    /**
     * Itera sobre els socis de la llista i llança un error al primer soci que falla la validacio.
     */
    @Override
    public void verificarSocis() throws ExcepcioClub {
        Iterator<Soci> it = _llista.iterator();
        while (it.hasNext()) {
            it.next().verifica();
        }
    }
}
