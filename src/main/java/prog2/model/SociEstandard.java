package prog2.model;

import prog2.vista.ExcepcioClub;

public class SociEstandard extends Soci {
    
    private Asseguranca _asseguranca;
    
    /**
     * @param _name
     * @param _dni
     * @param _asseguranca
     */
    public SociEstandard(String _name, String _dni, Asseguranca _asseguranca) {
        super(_name, _dni);
        this._asseguranca = _asseguranca;
        this.setQuotaBase(ClubSocial.CUOTA_ESTANDARD);
    }

    /**
     * @return the _asseguranca
     */
    public Asseguranca getAsseguranca() {
        return _asseguranca;
    }

    /**
     * @param asseguranca
     */
    public void setAsseguranca(Asseguranca asseguranca) {
        this._asseguranca = asseguranca;
    }

    /**
     * Calcula la factura del mes per al soci.
     */
    @Override
    public float calcularFactura() {
        return getQuotaBase() + calculaPreuActivitats() + getAsseguranca().getPreu();
    }

    
    @Override
    public String toString() {
        return String.format(
                "Nom=%s, DNI=%s. Assegurança del soci estàndard: Tipus=%s, Preu Assegurança=%.2f",
                getNom(), getDNI(), getAsseguranca().toString(),getAsseguranca().getPreu());
    }

    /**
     * Verifica la informacio del soci
     */
    @Override
    public void verifica() throws ExcepcioClub {
        if (getAsseguranca().toString().equals("Bàsica") || getAsseguranca().toString().equals("Completa")){ 
            throw new ExcepcioClub("El tipus d'assegurança no és correcte.");
        }
    }

    @Override
    public float calculaPreuActivitats() {
        return getActivitatsRealitzades().calculaPreuActivitats();
    }
}