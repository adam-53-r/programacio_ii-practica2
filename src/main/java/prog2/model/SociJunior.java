package prog2.model;

import prog2.vista.ExcepcioClub;

public class SociJunior extends SociEstandard {

    private int _edat;

    /**
     * @param _name
     * @param _dni
     * @param _asseguranca
     * @param _edat
     */
    public SociJunior(String _name, String _dni, Asseguranca _asseguranca, int _edat) {
        super(_name, _dni, _asseguranca);
        this._edat = _edat;
        this.setQuotaBase(ClubSocial.CUOTA_JUNIOR);
    }

    @Override
    public String toString() {
        return String.format(
                "Nom=%s, DNI=%s. Assegurança del soci junior: Tipus=%s, Preu Assegurança=%f, Edat=%d.",
                getNom(), getDNI(), getAsseguranca().toString(), getAsseguranca().getPreu(), this.getEdat()
            );
    }

    @Override
    public void verifica() throws ExcepcioClub {
        if (_edat > 18) {
            throw new ExcepcioClub("Soci Junior massa gran, edat > 18: " + this.toString());
        }
    }

    /**
     * @return the _edat
     */
    public int getEdat() {
        return _edat;
    }

    /**
     * @param edat the _edat to set
     */
    public void setEdat(int edat) {
        this._edat = edat;
    }


    
}
