package prog2.model;

import java.io.Serializable;

public class Activitat implements InActivitat, Serializable {

    // Al principi va intentar usar LocalDateTime per a guardar la data inici i final,
    // pero ens vam donar compte que ens estavem complicant massa.
    private String _name;
    private String _diaSetmana;
    private String _intervalHorari;
    private float _preu;
    
    /**
     * Constructor
     * @param _name
     * @param _diaSetmana
     * @param _intervalHorari
     * @param _preu
     */
    public Activitat(String _name, String _diaSetmana, String _intervalHorari, float _preu) {
        this._name = _name;
        this._diaSetmana = _diaSetmana;
        this._intervalHorari = _intervalHorari;
        this._preu = _preu;
    }
    

    // Métodos 
    @Override
    public String toString() {
         
        return String.format(
            "Activitat: %s, Dia: %s, Horari: %s, Preu: %.2f",
            _name, _diaSetmana, _intervalHorari, _preu
        );
    }

    /**
     * @return the _name
     */
    @Override
    public String getNom() {
        return _name;
    }

    /**
     * @param _name the _name to set
     */
    @Override
    public void setNom(String _name) {
        this._name = _name;
    }

    /**
     * @return the _diaSetmana
     */
    @Override
    public String getDiaSetmana() {
        return _diaSetmana;
    }

    /**
     * @param _diaSetmana the _diaSetmana to set
     */
    @Override
    public void setDiaSetmana(String _diaSetmana) {
        this._diaSetmana = _diaSetmana;
    }

    /**
     * @return the _intervalHorari
     */
    @Override
    public String getIntervalHorari() {
        return _intervalHorari;
    }

    /**
     * @param _intervalHorari the _intervalHorari to set
     */
    @Override
    public void setIntervalHorari(String _intervalHorari) {
        this._intervalHorari = _intervalHorari;
    }

    /**
     * @return the _preu
     */
    @Override
    public float getPreu() {
        return _preu;
    }

    /**
     * @param _preu the _preu to set
     */
    @Override
    public void setPreu(float _preu) {
        this._preu = _preu;
    }
    
    
    // Hemos utilizado LocalDateTime, pero se nos complicaba bastante, 
    // aquí todo el código que habíamos utilizado en un principio.
    // Hecho por Carlos Moya
    // @Override
    // public String get_diaSetmana() {
    //    return _startDate.getDayOfWeek().toString();
    // }
    
    // /* 
    // Se actualiza el día de la semana
    // */
    
    // @Override
    // public void set_diaSetmana(String diaSetmana) {
    //     diaSetmana = diaSetmana.toUpperCase();
    //     DayOfWeek nuevoDia = DayOfWeek.valueOf(diaSetmana);
        
    //     int diferencia = nuevoDia.getValue() - _startDate.getDayOfWeek().getValue();
    //     _startDate = _startDate.plusDays(diferencia);
    //     _endDate = _endDate.plusDays(diferencia);
       
    // }
    
    // @Override
    // public String get_intervalHorari() {
    //     return _startDate.getHour()+"-"+_endDate.getHour();
    // }

    // @Override
    // public void set_intervalHorari(String intervalHorari) {
    //     String[] partes = intervalHorari.split("\\-");
        
    //      int horaInicio = Integer.parseInt(partes[0]);
    //      int horaFin = Integer.parseInt(partes[1]);

    //      _startDate = _startDate.withHour(horaInicio).withMinute(0).withSecond(0).withNano(0);
    //      _endDate = _endDate.withHour(horaFin).withMinute(0).withSecond(0).withNano(0);
    // }    
}
