package prog2.vista;

import java.util.Scanner;
import java.awt.GraphicsEnvironment;
import java.io.File;
import javax.swing.JFileChooser;
import prog2.model.ClubSocial;

public class VistaClubSocial {

    private ClubSocial clubSocial;

    /**
     * @param clubSocial
     */
    public VistaClubSocial() {
        this.clubSocial = new ClubSocial();
    }

    /**
     * Enum d'opcions del menu
     */
    public static enum llistaOpcions {
        M_Opcio_DONAR_ALTA_SOCI,
        M_Opcio_MOSTRAR_LLISTA_SOCIS,
        M_Opcio_MOSTRAR_LLISTA_SOCIS_VIP,
        M_Opcio_MOSTRAR_LLISTA_SOCIS_ESTÀNDARD,
        M_Opcio_MOSTRAR_LLISTA_SOCIS_JUNIOR,
        M_Opcio_ELIMINAR_SOCI,
        M_Opcio_VERIFICAR_SOCIS,
        M_Opcio_MOSTRAR_LLISTA_ACTIVITATS,
        M_Opcio_MOSTRAR_LLISTA_ACTIVITATS_REALITZADES_PER_UN_SOCI,
        M_Opcio_AFEGIR_ACTIVITAT_REALITZADA_PER_UN_SOCI,
        M_Opcio_MOSTRAR_TOTAL_FACTURA,
        M_Opcio_MODIFICAR_NOM_SOCI,
        M_Opcio_MODIFICAR_TIPUS_ASSEGURANCA_SOCI_ESTANDARD,
        M_Opcio_GUARDAR_DADES,
        M_Opcio_RECUPERAR_DADES,
        M_Opcio_SORTIR
    };

    /**
     * Descripcions a imprimir per pantalla
     */
    String[] descripcions = {
        "Donar d'alta un nou soci",
        "Mostrar llista de socis",
        "Mostrar llista de socis vip",
        "Mostrar llista de socis estàndard",
        "Mostrar llista de socis junior",
        "Eliminar soci",
        "Verificar socis",
        "Mostrar llista d'activitats",
        "Mostrar llista d'activitats realitzades per un soci",
        "Afegir activitat realitzada per un soci",
        "Mostrar total factura",
        "Modificar nom soci",
        "Modificar tipus assegurança soci estàndard",
        "Guardar dades",
        "Recuperar dades",
        "Sortir"
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////// HELPER METHODS ///////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Imprimeix el missatge en verd.
     *
     * @param message
     */
    public void printSuccess(String message) {
        System.out.print(String.format("\033[32m%s\033[0m", message));
    }

    /**
     * Imprimeix el missatge en vermell.
     *
     * @param message
     */
    public void printError(String message) {
        System.out.print(String.format("\033[31m%s\033[0m", message));
    }

    /**
     * Li demana una entrada al usuari.
     *
     * @param sc
     * @param message
     * @return
     */
    public String input(Scanner sc, String message) {
        System.out.print(message);
        System.out.print("\033[32m>> \033[0m");
        return sc.nextLine();
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un int, sino ho
     * ses li demana una altra vegada
     *
     * @param sc
     * @param message
     * @return
     */
    public int inputInt(Scanner sc, String message) {
        
        int response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextInt();
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un int dintre
     * del rang especificat amb min i max, sino ho ses li demana una altra
     * vegada
     *
     * @param sc
     * @param message
     * @param min
     * @param max
     * @return
     */
    public int inputInt(Scanner sc, String message, int min, int max) {
        
        int response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextInt();
                if (response < min || response > max) {
                    printError(String.format("Error: Introdueix un numero entre %d i %d.\n", min, max));
                    continue;
                }
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un float, sino
     * ho ses li demana una altra vegada
     *
     * @param sc
     * @param message
     * @return
     */
    public float inputFloat(Scanner sc, String message) {
        
        float response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextFloat();
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un float dintre
     * del rang especificat amb min i max, sino ho ses li demana una altra
     * vegada
     *
     * @param sc
     * @param message
     * @param min
     * @param max
     * @return
     */
    public float inputFloat(Scanner sc, String message, float min, float max) {
        
        float response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextFloat();
                if (response < min || response > max) {
                    printError(String.format("Error: Introdueix un numero entre %f i %f.\n", min, max));
                    continue;
                }
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// METODES DEL MENU //////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Li demana el tipus de soci a crear, les dades del soci i intenta
     * afegir-ho, si hi ha algun error, el imprimeix per pantalla.
     *
     * @param sc
     */
    public void donarAltaSoci(Scanner sc) {

        int choice = inputInt(
            sc,
            "Sel·lecciona una opcio: (0-3)\n"
            + " [0] Afegir soci vip\n"
            + " [1] Afegir soci estandard\n"
            + " [2] Afegir soci junior\n"
            + " [3] Tornar al menu principal\n",
            0,
            3
        );

        // All
        String _name;
        String _dni;
        // Vip
        float _descompte;
        // Estandard i Junior
        String _tipusAsseguranca;
        int _tipusAssegurancaInput;
        float _preuAsseguranca;
        // Junior
        int _edat;

        try {

            switch (choice) {
                case 0:
                    _name = input(sc, "Introdueix el nom del soci:\n");
                    _dni = input(sc, "Introdueix el dni:\n");
                    _descompte = inputFloat(sc, "Introdueix el descompte: (0% a 100%)\n", 0.0f, 100.0f);
                    clubSocial.donarAltaSociVip(_name, _dni, _descompte);
                    break;

                case 1:
                    _name = input(sc, "Introdueix el nom del soci:\n");
                    _dni = input(sc, "Introdueix el dni:\n");
                    _tipusAssegurancaInput = inputInt(
                        sc,
                        "Sel·lecciona un tipus d'assegurança: (0-1)\n"
                        + " [0] Assegurança bàsica\n"
                        + " [1] Assegurança completa\n",
                        0,
                        1
                    );
                    if (_tipusAssegurancaInput == 0) {
                        _tipusAsseguranca = "BASICA";
                    } else {
                        _tipusAsseguranca = "COMPLETA";
                    }
                    _preuAsseguranca = inputFloat(sc, "Introdueix el preu de l'assegurança:\n");
                    clubSocial.donarAltaSociEstandard(_name, _dni, _tipusAsseguranca, _tipusAssegurancaInput, _preuAsseguranca);
                    break;

                case 2:
                    _name = input(sc, "Introdueix el nom del soci:\n");
                    _dni = input(sc, "Introdueix el dni:\n");
                    _tipusAssegurancaInput = inputInt(
                        sc,
                        "Sel·lecciona un tipus d'assegurança: (0-1)\n"
                        + " [0] Assegurança bàsica\n"
                        + " [1] Assegurança completa\n",
                        0,
                        1
                    );
                    if (_tipusAssegurancaInput == 0) {
                        _tipusAsseguranca = "BASICA";
                    } else {
                        _tipusAsseguranca = "COMPLETA";
                    }
                    _preuAsseguranca = inputFloat(sc, "Introdueix el preu de l'assegurança:\n");
                    _edat = inputInt(sc, "Introdueix la edat del soci junior:\n");
                    clubSocial.donarAltaSociJunior(_name, _dni, _tipusAsseguranca, _tipusAssegurancaInput, _preuAsseguranca, _edat);
                    break;

                case 3:
                    printError("Back to the main menu.\n");
                    break;
            }
            printSuccess("Soci afegit correctament.\n");
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage() + "\n");
        }

    }

    /**
     * Mostra una llista amb tots els socis del club.
     */
    public void mostrarLlistaSocis() {
        
        try {
            System.out.println(clubSocial.mostrarLlistaSocisByType("all"));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage());
        }
    }

    /**
     * Mostra una llista amb tots els socis VIP del club.
     */
    public void mostrarLlistaSocisVip() {

        try {
            System.out.println(clubSocial.mostrarLlistaSocisByType("SociVip"));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage());
        }

    }

    /**
     * Mostra una llista amb tots els socis estandard del club.
     */
    public void mostrarLlistaSocisEstandard() {

        try {
            System.out.println(clubSocial.mostrarLlistaSocisByType("SociEstandard"));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage());
        }

    }

    /**
     * Mostra una llista amb tots els socis junior del club.
     */
    public void mostrarLlistaSocisJunior() {

        try {
            System.out.println(clubSocial.mostrarLlistaSocisByType("SociJunior"));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage());
        }

    }


    /**
     * Mostra la llista de tots els socis i demana el dni d'un soci per eliminar-lo de la llista de les activitats que realitza. 
     * @param sc
     */
    public void eliminarSoci(Scanner sc) {

        if (clubSocial.getNumSocis() == 0) {
            printError("No hi ha socis per esborrar");
        } else {

            mostrarLlistaSocis();
            String dni = input(sc, "Introdueix el dni del soci a eliminar:\n");
            try {
                clubSocial.eliminarSoci(dni);
                printSuccess("El soci s'ha eliminat correctament.\n");
            } catch (ExcepcioClub e) {
                printError("Error: " + e.getMessage() + "\n");
            }
        }
    }

    /**
     * Intenta verificar els socis, al primer soci que no verifica, imprimeix el
     * error per pantalla.
     */
    public void verificarSocis() {
        try {
            clubSocial.verificarSocis();
            printSuccess("Tots els socis son correctes.\n");
        }
        catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage() + "\n");
        }
    }

    /**
     * Mostra una llista de les activitats del club per pantalla.
     */
    public void mostrarLlistaActivitats() {
        System.out.println(clubSocial.mostrarLlistaActivitats());
    }
    
    /**
     * Mostra la llista de tots els socis i demana el dni d'un soci per mostrar la llista de les activitats que realitza.
     * @param sc
     */
    public void mostrarLlistaActivitatsRealitzadesPerUnSoci(Scanner sc) {
        mostrarLlistaSocis();
        String dni = input(sc, "Introdueix el dni del soci desitjat:\n");
        try {
            System.out.println(clubSocial.mostrarLlistaActivitatsRealitzadesPerUnSoci(dni));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage() + "\n");
        }

    }

    /**
     * Mostra la llista de tots els socis i demana el dni d'un soci per afegir una activitat a la llista de les activitats que realitza.
     * @param sc
     */
    public void afegirActivitatRealitzadaPerUnSoci(Scanner sc) {
        mostrarLlistaSocis();
        String dni = input(sc, "Introdueix el dni del soci desitjat:\n");

        mostrarLlistaActivitats();
        int indexActivitat = inputInt(sc, "Introdueix el numero de la activitat que vols afegir:\n");

        try {
            clubSocial.afegirActivitatRealitzadaPerUnSoci(dni, indexActivitat);
            printSuccess("La activitat s'ha afegit correctament.\n");
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage() + "\n");
        }
    }

    /**
     * Mostra la llista de tots els socis i demana el dni d'un soci per calcular
     * la seva factura del mes.
     * @param sc
     */
    public void mostrarTotalFactura(Scanner sc) {
        mostrarLlistaSocis();
        String dni = input(sc, "Introdueix el dni del soci desitjat:\n");
        try {
            System.out.println(String.format("Total de la factura del client amb DNI \"%s\": %.2f€", dni, clubSocial.totalFacturaSoci(dni)));
        } catch (ExcepcioClub e) {
            printError("Error: " + e.getMessage() + "\n");
        }
    }

    /**
     * Demana el dni del soci a modificar, llavors demana el nou nom a establir.
     * @param sc
     */
    public void modificarNomSoci(Scanner sc) {
        
        if (clubSocial.getNumSocis() == 0) {
            printError("No hi ha socis per modificar");
        } else {
            mostrarLlistaSocis();
            String dni = input(sc, "Introdueix el dni del soci desitjat:\n");
            String newName = input(sc, "Introdueix el nou nom:\n");
            try {
                clubSocial.modificarNomSoci(dni, newName);
                printSuccess("El nom s'ha canviat correctament.\n");
            } catch (ExcepcioClub e) {
                printError("Error: " + e.getMessage() + "\n");
            }
        }
    }

    /**
     * Demana el dni del soci a modificar i llavors demana el nou tipus d'assegurança a establir
     * @param sc
     */
    public void modificarTipusAssegurancaSociEstandard(Scanner sc) {
        
        if (clubSocial.getNumSocis() == 0) {
            printError("No hi ha socis per modificar");
        } else {
            mostrarLlistaSocisEstandard();
            String dni = input(sc, "Introdueix el dni del soci desitjat:\n");
            int _tipusAssegurancaInput = inputInt(
                sc,
                "Sel·lecciona un tipus d'assegurança: (0-1)\n"
                + " [0] Assegurança bàsica\n"
                + " [1] Assegurança completa\n",
                0,
                1
            );
            String _tipusAsseguranca;
            if (_tipusAssegurancaInput == 0) {
                _tipusAsseguranca = "BASICA";
            } else {
                _tipusAsseguranca = "COMPLETA";
            }
            try {
                clubSocial.modificarTipusAssegurancaSociEstandard(dni, _tipusAsseguranca);
            } catch (ExcepcioClub e) {
                printError("Error: " + e.getMessage() + "\n");
            }
        }
    }

    /**
     * Intenta obrir una finestra per a sel·leccionar el arxiu a guardar i
     * guardar el club social, sino pot, demana la ruta del fitxer per la
     * terminal.
     * Al executar el programa en Netbeans, es possible que no funcioni, 
     * que el programa es quedi al limbo sense mostrar cap finestra pero 
     * sense donar cap error. Nose perque passa, pero nomes passa en Netbeans.
     * @param sc
     */
    public void guardarDades(Scanner sc) {

        if (!GraphicsEnvironment.isHeadless()) {

            // Create a new file chooser
            JFileChooser fileChooser = new JFileChooser();

            // Set the file chooser to "Save" mode
            fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);

            while (true) {
                // Set the current directory of the chooser
                fileChooser.setCurrentDirectory(new File("."));
                // Show the system "Save As" dialog
                fileChooser.showSaveDialog(null);
                try {
                    // Get the selected file
                    File file = fileChooser.getSelectedFile();
                    file.canRead();
                    file.canWrite();
                    clubSocial.saveToFile(file);
                    printSuccess("Info saved successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        } else {

            while (true) {
                try {
                    File file = new File(input(sc, "Save path (file):\n"));
                    file.canRead();
                    file.canWrite();
                    clubSocial.saveToFile(file);
                    printSuccess("Info saved successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        }
    }

public void recuperarDades(Scanner sc) {
    
    if (!GraphicsEnvironment.isHeadless()) {

        // Create a new file chooser
        JFileChooser fileChooser = new JFileChooser();

        // Set the file chooser to "Save" mode
        fileChooser.setDialogType(JFileChooser.FILES_ONLY);

        while (true) {
            // Set the current directory of the chooser
            fileChooser.setCurrentDirectory(new File("."));
            // Show the system "Open" dialog
            fileChooser.showSaveDialog(null);
            try {
                // Get the selected file
                File file = fileChooser.getSelectedFile();
                file.canRead();
                clubSocial.loadFromFile(file);
                printSuccess("Info loaded successfully.\n");
                break;
            } catch (Exception e) {
                printError("Error: Can't use the file.\n");
                e.printStackTrace();
                if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                    break;
                }
            }
        }
    } else {

        while (true) {
            try {
                File file = new File(input(sc, "Open path (file):\n"));
                file.canRead();
                clubSocial.loadFromFile(file);
                printSuccess("Info loaded successfully.\n");
                break;
            } catch (Exception e) {
                printError("Error: Can't use the file.\n");
                e.printStackTrace();
                if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                    break;
                }
            }
        }
    }
}


    public void gestioClubSocial() {

        Scanner sc = new Scanner(System.in);

        Menu<llistaOpcions> menu = new Menu<>("Club Social", llistaOpcions.values());

        menu.setDescripcions(descripcions);

        llistaOpcions opcio;

        while (true) {
            menu.mostrarMenu();
            opcio = menu.getOpcio(sc);

            switch (opcio) {
                case M_Opcio_DONAR_ALTA_SOCI:
                    donarAltaSoci(sc);
                    break;

                case M_Opcio_MOSTRAR_LLISTA_SOCIS:
                    mostrarLlistaSocis();
                    break;

                case M_Opcio_MOSTRAR_LLISTA_SOCIS_VIP:
                    mostrarLlistaSocisVip();
                    break;

                case M_Opcio_MOSTRAR_LLISTA_SOCIS_ESTÀNDARD:
                    mostrarLlistaSocisEstandard();
                    break;

                case M_Opcio_MOSTRAR_LLISTA_SOCIS_JUNIOR:
                    mostrarLlistaSocisJunior();
                    break;

                case M_Opcio_ELIMINAR_SOCI:
                    eliminarSoci(sc);
                    break;

                case M_Opcio_VERIFICAR_SOCIS:
                    verificarSocis();
                    break;

                case M_Opcio_MOSTRAR_LLISTA_ACTIVITATS:
                    mostrarLlistaActivitats();
                    break;

                case M_Opcio_MOSTRAR_LLISTA_ACTIVITATS_REALITZADES_PER_UN_SOCI:
                    mostrarLlistaActivitatsRealitzadesPerUnSoci(sc);
                    break;

                case M_Opcio_AFEGIR_ACTIVITAT_REALITZADA_PER_UN_SOCI:
                    afegirActivitatRealitzadaPerUnSoci(sc);
                    break;

                case M_Opcio_MOSTRAR_TOTAL_FACTURA:
                    mostrarTotalFactura(sc);
                    break;

                case M_Opcio_MODIFICAR_NOM_SOCI:
                    modificarNomSoci(sc);
                    break;

                case M_Opcio_MODIFICAR_TIPUS_ASSEGURANCA_SOCI_ESTANDARD:
                    modificarTipusAssegurancaSociEstandard(sc);
                    break;

                case M_Opcio_GUARDAR_DADES:
                    guardarDades(sc);
                    break;

                case M_Opcio_RECUPERAR_DADES:
                    recuperarDades(sc);
                    break;

                case M_Opcio_SORTIR:
                    return;

                default:
                    break;
            }
        }

    }
}
