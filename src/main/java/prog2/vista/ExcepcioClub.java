package prog2.vista;

public class ExcepcioClub extends Throwable {

    public ExcepcioClub() {
    }

    /**
     * @param message
     */
    public ExcepcioClub(String message) {
        super(message);
    }
}
